# SusMet_thermodynamics

### Prebuilt session

[![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fsusmet_thermodynamics.git/8ac1c2fbc3407d31627d578e3e5685a4faba332d)

short url: https://s.gwdg.de/q8LPYX

Add MD content

```git clone https://gitlab.mpcdf.mpg.de/smenon/atomistics.git```

### Latest repo state

[![Binder](https://notebooks.mpcdf.mpg.de/binder/badge_logo.svg)](https://notebooks.mpcdf.mpg.de/binder/v2/git/https%3A%2F%2Fgitlab.mpcdf.mpg.de%2Fpyiron%2Fsusmet_thermodynamics.git/HEAD)

short url: https://s.gwdg.de/0wCqjw